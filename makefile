.PHONY: force

all: output.pdf

output.pdf: *.md Matos/*
	pandoc *.md \
		--standalone \
		--toc --toc-depth=3 \
		--number-sections \
		--css Matos/csspourpdf.css \
		--bibliography Matos/biblio.bib \
		--csl Matos/iso690-author-date-fr-no-abstract.csl \
		-t html \
		-o tempo.html \
		| weasyprint tempo.html output.pdf

force:
	@rm output.pdf
	@make
