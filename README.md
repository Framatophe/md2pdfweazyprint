# md2pdfweazyprint

Markdown to PDF sans LaTeX pas trop naze mais pratique.

Pourquoi ?
-------------

Expression du besoin : transformer un fichier
markdown en PDF en utilisant pandoc mais sans utiliser LaTeX.

Être allergique à la ligne de commande mais accepter de copier
et coller des commandes de temps à autre pourvu que ce soit facile.

Solution : utiliser weasyprint, un modèle CSS pour faire joli, 
possibilité d'utiliser de la bibliographie et un format CSL

Pandoc
-----------

Il faut installer `pandoc` et `pandoc-citeproc`.

(c'est dans les dépôts)


Weasyprint
-----------------

Il faut installer `weasyprint`.

(c'est dans les dépôts, voir le [site officiel](https://weasyprint.org/))


Comment faire ?
----------------------

Vous écrivez et enregistrez dans un dossier votre document
en markdown, vous y mettez aussi vos illustrations si vous en avez.

Vous placez dans ce même dossier :

- le dossier `\Matos` 
- et le fichier `makefile`.

Vous ouvrez un terminal et allez dans ce dossier (sous Ubuntu,
vous faites clic-droit et « ouvrir dans un terminal »).

Vous tapotez `make` puis `Entrée`.

Et là ça compile votre fichier et vous voilà avec un fichier `tempo.html` et un fichier `output.pdf` dans le même dossier.

Si vous n’avez pas modifié de fichiers depuis la dernière compilation, il n’y aura pas de recompilation.

Si vous avez supprimé un fichier depuis la dernière compilation, ça ne sera pas détecté.
Il vous faudra soit supprimer le fichier `output.pdf`, soit écrire `make force` puis appuyer sur la touche `Entrée`.


Comment bidouiller ?
---------------------

Dans le dossier `\Matos` vous pouvez changer les fichiers :

- .css (si vous voulez l'améliorer)
- .csl (le mieux est d'aller chercher un CSL à votre convenance et rempalcer son nom dans le `makefile`
- .bib (c'est votre biblio)

Ouvrez le `makefile` :

- vous pouvez changer les option pandoc
- vous pouvez changer les noms de fichier utilisés
